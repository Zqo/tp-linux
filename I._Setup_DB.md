# I. Setup DB
## 1. Install MariaDB
**🌞 Installer MariaDB sur la machine db.tp5.linux**
```bash=
[zqo@db ~]$ sudo dnf install mariadb-server
Rocky Linux 8 - AppStream                        13 kB/s | 4.8 kB     00:00    
Rocky Linux 8 - AppStream                       6.3 MB/s | 8.2 MB     00:01  
[...]
  psmisc-23.1-5.el8.x86_64                               

Terminé !
```

**🌞 Le service MariaDB**
* Lancez-le avec une commande systemctl
```bash=
[zqo@db ~]$ sudo systemctl start mariadb
[sudo] Mot de passe de zqo : 
```
* Exécutez la commande sudo systemctl enable mariadb
```bash=
[zqo@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
* Vérifiez qu'il est bien actif 
```bash=
[zqo@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor pre>
   Active: active (running) since Fri 2021-11-26 08:33:28 EST; 1min 51s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 4668 (mysqld)
```
* Déterminer sur quel port la base de données écoute
```bash=
[zqo@db ~]$ sudo ss -ltpn | grep mysql
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=4668,fd=21))
```
* Isolez les processus liés au service MariaDB
```bash=
[zqo@db ~]$ sudo ps -ef | grep mysql
mysql       4668       1  0 08:33 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
zqo         4784    1517  0 08:45 pts/0    00:00:00 grep --color=auto mysql
```
**🌞 Firewall**
```bash=
[zqo@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
```
```bash=
[zqo@db ~]$ sudo firewall-cmd --reload
success
```
## 2. Conf MariaDB
**🌞 Configuration élémentaire de la base**
* Exécutez la commande mysql_secure_installation
```bash=
[zqo@db ~]$ mysql_secure_installation
```
```bash=
Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y

Afin d'avoir une sécurité supplémentaire
```
```bash=
By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y

Car nous entrons dans un univers de production
```
```bash=
Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y

Afin d'éviter la fuite du mdp
```
```bash=
By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y

Car nous entrons dans un univers de production
```
```bash=
Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
```

**🌞 Préparation de la base en vue de l'utilisation par NextCloud**

```bash=
[zqo@db ~]$ sudo mysql -u root -p
[sudo] Mot de passe de zqo : 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.
```
* Puis, dans l'invite de commande SQL :
```bash=
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.002 sec)
```
```bash=
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)
```
```bash=
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.001 sec)
```
```bash=
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
## 3. Test
**🌞 Installez sur la machine web.tp5.linux la commande mysql**
```bash=
[zqo@db ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                       3.6 MB/s | 8.2 MB     00:02    
Rocky Linux 8 - BaseOS                          2.2 MB/s | 3.5 MB     00:01    
Rocky Linux 8 - Extras                          9.6 kB/s |  10 kB     00:01    
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:00:01 le ven. 26 nov. 2021 09:52:46 EST.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and
                                                  : shared libraries
Dépôt               : appstream
Correspondances trouvées dans  :
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```
**🌞 Tester la connexion**

```bash=
[zqo@web ~]$ sudo mysql -u nextcloud -p -P 3306 -D nextcloud -h 10.5.1.12
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.
```
```bash=
[zqo@web ~]$ sudo dnf install mysql
Rocky Linux 8 - AppStream                       6.3 kB/s | 4.8 kB     00:00    
Rocky Linux 8 - AppStream                       2.8 MB/s | 8.2 MB     00:02    
Rocky Linux 8 - BaseOS                          6.1 kB/s | 4.3 kB     00:00    
Rocky Linux 8 - BaseOS                          754 kB/s | 3.5 MB     00:04  
[...]
Installé:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                              
  mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                             
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                      

Terminé !
```
# II. Setup Web

## 1. Install Apache

### A. Apache
**🌞 Installer Apache sur la machine web.tp5.linux**
```bash=
[zqo@db ~]$ sudo dnf install httpd
[sudo] Mot de passe de zqo : 
Dernière vérification de l’expiration des métadonnées effectuée il y a 4:17:19 le ven. 26 nov. 2021 10:51:55 EST.
Dépendances résolues.
[...]
Installé:
  apr-1.6.3-12.el8.x86_64                                                       
  apr-util-1.6.1-6.el8.1.x86_64                                                 
  apr-util-bdb-1.6.1-6.el8.1.x86_64                                             
  apr-util-openssl-1.6.1-6.el8.1.x86_64                                         
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                            
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                 
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                      
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                         
  rocky-logos-httpd-85.0-3.el8.noarch                                           

Terminé !
```
**🌞 Analyse du service Apache**
```bash=
[zqo@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentification requise pour démarrer « httpd.service ».
Authenticating as: zqo
Password: 
==== AUTHENTICATION COMPLETE ====
```
```bash=
[zqo@web ~]$ systemctl enable httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: zqo
Password: 
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: zqo
Password: 
==== AUTHENTICATION COMPLETE ====
```
```bash=
[zqo@db ~]$ sudo ss -ltpn | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=5680,fd=4),("httpd",pid=5679,fd=4),("httpd",pid=5678,fd=4),("httpd",pid=5676,fd=4))
```
```bash=
[zqo@web ~]$ sudo ps -ef | grep httpd
root        5676       1  0 15:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      5677    5676  0 15:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      5678    5676  0 15:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      5679    5676  0 15:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      5680    5676  0 15:34 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
zqo         5898    5139  0 15:35 pts/1    00:00:00 grep --color=auto httpd
```
```bash=
[zqo@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor prese>
   Active: active (running) since Fri 2021-11-26 15:34:42 EST; 6min ago
     Docs: man:httpd.service(8)
 Main PID: 5676 (httpd)
   Status: "Running, listening on: port 80"
```
**🌞 Un premier test**
```bash=
[zqo@web ~]$ curl 10.5.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
  </body>
</html>
```
### B. PHP
**🌞 Installer PHP**
```bash=
[zqo@web ~]$ sudo dnf install epel-release
[sudo] Mot de passe de zqo : 
Dernière vérification de l’expiration des métadonnées effectuée il y a 1:08:05 le ven. 26 nov. 2021 16:23:03 EST.
[...]
Installé:
  epel-release-8-13.el8.noarch                                                  
  
Terminé !
```
```bash=
[zqo@web ~]$ sudo dnf update
[sudo] Mot de passe de zqo : 
Extra Packages for Enterprise Linux 8 - x86_64  8.9 MB/s |  11 MB     00:01    
Extra Packages for Enterprise Linux Modular 8 - 2.0 MB/s | 958 kB     00:00    
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:00:01 le ven. 26 nov. 2021 18:12:31 EST.
Dépendances résolues.
Rien à faire.
Terminé !
```
```bash=
[zqo@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:02:04 le ven. 26 nov. 2021 18:12:31 EST.
remi-release-8.rpm                              157 kB/s |  26 kB     00:00    
Dépendances résolues.
================================================================================
 Paquet             Architecture Version               Dépôt              Taille
================================================================================
Installation:
 remi-release       noarch       8.5-2.el8.remi        @commandline        26 k

Résumé de la transaction
================================================================================
Installer  1 Paquet

Taille totale  : 26 k
Taille des paquets installés : 21 k
Voulez-vous continuer ? [o/N] : o
Téléchargement des paquets :
Test de la transaction
La vérification de la transaction a réussi.
Lancement de la transaction de test
Transaction de test réussie.
Exécution de la transaction
  Préparation           :                                                   1/1 
  Installation          : remi-release-8.5-2.el8.remi.noarch                1/1 
  Vérification de       : remi-release-8.5-2.el8.remi.noarch                1/1 

Installé:
  remi-release-8.5-2.el8.remi.noarch                                            

Terminé !
```
```bash=
[zqo@web ~]$ dnf module enable php:remi-7.4
Activation des flux de modules:
 php                                remi-7.4                                   

Résumé de la transaction
================================================================================

Voulez-vous continuer ? [o/N] : o
Terminé !
```
```bash=
[zqo@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
[...]
Dépendances résolues.
Rien à faire.
Terminé !
```
## 2. Conf Apache

**🌞 Analyser la conf Apache**

```bash=
[zqo@web ~]$ cat /etc/httpd/conf/httpd.conf | grep conf.d
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```
**🌞 Créer un VirtualHost qui accueillera NextCloud**
```bash=
[zqo@web ~]$ cat  /etc/httpd/conf.d/nextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

```
**🌞 Configurer la racine web**
```bash=
[zqo@web ~]$ sudo mkdir /var/www/nextcloud/
[sudo] Mot de passe de zqo : 
[zqo@web ~]$ cd /var/www/nextcloud/
[zqo@web nextcloud]$ sudo mkdir html
[zqo@web nextcloud]$ ls
html
```
```bash=
[zqo@web ~]sudo chown apache /var/www/nextcloud/
[zqo@web ~]$ cd /var/www
[zqo@web www]$ ls -la 
total 4
drwxr-xr-x.  5 root   root   50  3 déc.  06:14 .
drwxr-xr-x. 22 root   root 4096 26 nov.  15:09 ..
drwxr-xr-x.  2 root   root    6 14 nov.  22:13 cgi-bin
drwxr-xr-x.  2 root   root    6 14 nov.  22:13 html
drwxr-xr-x.  3 apache root   18  3 déc.  06:15 nextcloud
```
**🌞 Configurer PHP**
```bash=
[zqo@web www]$ timedatectl
               Local time: ven. 2021-12-03 06:32:35 EST
           Universal time: ven. 2021-12-03 11:32:35 UTC
                 RTC time: ven. 2021-12-03 11:32:33
                Time zone: America/New_York (EST, -0500)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
[zqo@web www]$ sudo nano /etc/opt/remi/php74/php.ini
[sudo] Mot de passe de zqo : 
[zqo@web www]$ cat /etc/opt/remi/php74/php.ini | grep timezone
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone = "America/New_York"
```
## 3. Install NextCloud
**🌞 Récupérer Nextcloud**
```bash=
[zqo@web www]$ cd
[zqo@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  27.6M      0  0:00:05  0:00:05 --:--:-- 31.0M
[zqo@web ~]$ ls
nextcloud-21.0.1.zip
```
**🌞 Ranger la chambre**
```bash=
[zqo@web ~]sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/
[zqo@web ~]sudo mv /var/www/nextcloud/html/nextcloud/ /var/www/nextcloud/
```
## 4.Test
**🌞 Modifiez le fichier hosts de votre PC**
```bash=
theorivry@theos-mbp ~ % cat /etc/hosts
[...]
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost
10.5.1.11       web.tp5.linux  
```
**🌞 Tester l'accès à NextCloud et finaliser son install'**
```bash=
[zqo@web ~]$ curl http://web.tp5.linux
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL was not found on this server.</p>
</body></html>
```