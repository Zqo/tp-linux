Output : 


IDCARD
```
zqo@zqo:~/srv/idcard$ sh idcard.sh
Name : zqo-VirtualBox
OS Linux and kernel version is 5.11.0-38-generic
IP : 192.168.57.7/24
RAM :  kB/ 2032944 Ko
Disque : 2,6G left
Top 5 processes by RAM usage :
- %MEM CMD                             PID
-  5.1 /usr/lib/xorg/Xorg -core :0     576
-  4.1 xfwm4 --replace                 941
-  2.2 /usr/bin/python3 /usr/bin/b    1012
-  2.2 xfdesktop                       971
-  2.1 /usr/lib/x86_64-linux-gnu/x     970
Listening ports :
- 53 :
- 22 :
- 631 :
- 22 :
- 631 :

Here's your random cat : https://cdn2.thecatapi.com/images/ahb.jpg
```
Youtube-dl :

Si les dossier downloads et /var/log/yt sont présents ->
```
zqo@zqo:/srv/yt$ sudo bash yt.sh https://www.youtube.com/watch?v=dQw4w9WgXcQ
Video https://www.youtube.com/watch?v=dQw4w9WgXcQ was downloaded.
File Path : /srv/yt/downloads/Rick Astley - Never Gonna Give You Up (Official Music Video)/Rick Astley - Never Gonna Give You Up (Official Music Video)
```

Si il en manque un (exemple le dossier downloads) ->
```
zqo@zqo:~/Tp-Linux/TP3/srv/yt$ ls
yt.sh
zqo@zqo:~/Tp-Linux/TP3/srv/yt$ sudo bash yt.sh https://www.youtube.com/watch?v=jjs27jXL0Zs&ab_channel=REDD%C3%A9fis
Dossier downloads non existant
```

Si il y a une erreur et que la vidéo n'existe pas ->
```
zqo@zqo:~/Tp-Linux/TP3/srv/yt$ sudo bash yt.sh https://www.youtube.com/watch?v=titouanvaaleclerc
The link is wrong dummie
```

Youtube-dl Service :

Je le lance : 

```
 yt.service - YT service
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-24 02:02:27 CET; 5s ago
   Main PID: 2622 (sudo)
      Tasks: 3 (limit: 2312)
     Memory: 1.4M
     CGroup: /system.slice/yt.service
             ├─2622 /usr/bin/sudo bash /srv/yt/yt-v2.sh
             ├─2623 bash /srv/yt/yt-v2.sh
             └─2624 sleep 5s

nov. 24 02:02:27 zqo systemd[1]: Started YT service.
nov. 24 02:02:27 zqo sudo[2622]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/bash /srv/yt>
nov. 24 02:02:27 zqo sudo[2622]: pam_unix(sudo:session): session opened for user root by (uid=0)
```

Le service fonctionne pour l'instant.

```
zqo@zqo:~/Tp-Linux/TP3/srv/yt-v2$ echo "https://www.youtube.com/watch?v=yPE0OHogJgc" >> video-urls
zqo@zqo:~/Tp-Linux/TP3/srv/yt-v2$ cat video-urls
https://www.youtube.com/watch?v=yPE0OHogJgc
```

Le premier lien a été téléchargé et est donc supprimé Les logs ->
```
zqo@zqo:~/Tp-Linux/TP3/srv/yt-v2$ tail -f /var/log/yt/download.log 
[11/24/21 02:07:58] Video https://www.youtube.com/watch?v=PE0OHogJgc was downloaded. File Path : /srv/yt/downloads/Top 10 Reasons Why Cats are Better than Dogs/Top 10 Reasons Why Cats are Better than Dogs
```

le journal:

```
zqo@zqo:journalctl -xe -u yt
    -- The job identifier is 3507.
    nov. 24 02:15:03 zqo bash[4802]: Video https://www.youtube.com/watch?v=PE0OHogJgc was downloaded
    nov. 24 02:15:03 zqo bash[4802]: File path : /srv/yt/downloads/op 10 Reasons Why Cats are Better than Dogs/Top 10 Reasons Why Cats are Better than Dogs
```


