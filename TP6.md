# Partie 1 : Préparation de la machine backup.tp6.linux

## I. Ajout de disque

**🌞 Ajouter un disque dur de 5Go à la VM backup.tp6.linux**
```bash=
[zqo@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    5G  0 disk
sr0          11:0    1 1024M  0 rom
```
## II. Partitioning
**🌞 Partitionner le disque à l'aide de LVM**
```bash=
[zqo@backup ~]$ sudo pvcreate /dev/sdb
[sudo] Mot de passe de zqo : 
  Physical volume "/dev/sdb" successfully created.
```
```bash=
[zqo@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7,00g    0 
  /dev/sdb      lvm2 ---   5,00g 5,00g
```
```bash=
[zqo@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
[zqo@db ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree 
  backup   1   0   0 wz--n- <5,00g <5,00g
  rl       1   2   0 wz--n- <7,00g     0`
```
```bash=
[zqo@backup ~]$ sudo lvcreate -l 100%FREE backup -n data
  Logical volume "data" created.
  ```
```bash=
[zqo@backup ~]$ sudo lvs
  LV   VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data backup -wi-a-----  <5,00g                                                    
  root rl     -wi-ao----  <6,20g                                                    
  swap rl     -wi-ao---- 820,00m                                                    
 ```
 
 **🌞 Formater la partition**
 ```bash=
 [zqo@backup ~]$ sudo mkfs -t ext4 /dev/backup/data
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 1309696 4k blocs et 327680 i-noeuds.
UUID de système de fichiers=86bc12bb-cba7-46a5-a6b4-c6d93608040a
Superblocs de secours stockés sur les blocs : 
	32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocation des tables de groupe : complété                        
Écriture des tables d'i-noeuds : complété                        
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```
```bash=
[zqo@backup ~]$ sudo lvcreate -l 100%FREE backup -n data
  Calculated size of logical volume is 0 extents. Needs to be larger.
  ```
  **🌞 Monter la partition**
  ```bash=
[zqo@backup ~]$ sudo mkdir /mnt/backup
```
```bash=
[zqo@backup ~]$ sudo mount /dev/backup/data /mnt/backu`
```
```bash=
[zqo@backup ~]$ df -h
Sys. de fichiers        Taille Utilisé Dispo Uti% Monté sur
devtmpfs                  388M       0  388M   0% /dev
tmpfs                     405M       0  405M   0% /dev/shm
tmpfs                     405M    5,6M  400M   2% /run
tmpfs                     405M       0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root       6,2G    2,5G  3,8G  39% /
/dev/sda1                1014M    265M  750M  27% /boot
tmpfs                      81M       0   81M   0% /run/user/1000
/dev/mapper/backup-data   4,9G     20M  4,6G   1% /mnt/backu`
```
```bash=
[zqo@backup ~]$ sudo nano /mnt/backup/tes`
```
```bash=
[zqo@backup ~]$ sudo cat /mnt/backup/test
...
```
```bash=
[zqo@backup ~]$ cat /etc/fstab
...
/dev/mapper/backup-data /mnt/backup ext4 defaults 0 0
```
```bash=
[zqo@backup ~]$ sudo mount -av
/                        : ignoré
/boot                    : déjà monté
none                     : ignoré
/mnt/backup              : déjà monté
```
# Partie 2 : Setup du serveur NFS sur backup.tp6.linux
**🌞 Préparer les dossiers à partager**
```bash=
[zqo@backup ~]$ sudo mkdir /mnt/backup/web.tp6.linux
[sudo] Mot de passe de zqo :
```
```bash=
[zqo@backup ~]$ sudo mkdir /mnt/backup/db.tp6.linux
[sudo] Mot de passe de zqo :
```
**🌞 Install du serveur NFS**
```bash=
[zqo@backup ~]$ sudo dnf install nfs-utils
[...]
Installé:
  gssproxy-0.8.0-19.el8.x86_64         keyutils-1.5.10-9.el8.x86_64             
  libevent-2.1.8-5.el8.x86_64          libverto-libevent-0.3.0-5.el8.x86_64     
  nfs-utils-1:2.3.3-46.el8.x86_64      rpcbind-1.2.5-8.el8.x86_64               

Terminé !
```
**🌞 Conf du serveur NFS**
```bash=
[zqo@backup ~]$ cat /etc/idmapd.conf
[...]
Domain = tp6.linux
```
```bash=
[zqo@backup ~]$ sudo cat /etc/exports
/backup/web.tp6.linux/ 192.168.1.11/24(rw,no_root_squash)
/backup/db.tp6.linux/ 192.168.1.12/24(rw,no_root_squash)
```
rw = r-> droit de lecture et d'écriture 
no_root_squash = a les droits de root sur le répertoire
**🌞 Démarrez le service**
```bash=
[zqo@backup ~]$ sudo systemctl start nfs-server
```
```bash=
[zqo@backup ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor>
   Active: active (exited) since Wed 2021-12-22 12:22:06 EST; 5s ago
  Process: 27005 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then >
  Process: 26993 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 26992 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=1/FAIL>
 Main PID: 27005 (code=exited, status=0/SUCCESS)

déc. 22 12:22:06 db.tp6.linux systemd[1]: Starting NFS server and services...
déc. 22 12:22:06 db.tp6.linux exportfs[26992]: exportfs: Failed to stat /backup>
déc. 22 12:22:06 db.tp6.linux exportfs[26992]: exportfs: Failed to stat /backup>
déc. 22 12:22:06 db.tp6.linux systemd[1]: Started NFS server and services.
lines 1-12/12 (END)
```
```bash=
[zqo@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

```
**🌞 Firewall**
```bash=
[zqo@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
```
```bash=
[zqo@backup ~]$ sudo firewall-cmd --reload
success
```
```bash=
[zqo@backup ~]$ sudo ss -alntp
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process                                                                         
LISTEN    0         64                 0.0.0.0:2049             0.0.0.0:*                                                                                       
LISTEN    0         64                 0.0.0.0:37123            0.0.0.0:*                                                                                       
LISTEN    0         128                0.0.0.0:60905            0.0.0.0:*        users:(("rpc.statd",pid=26983,fd=10))                                          
LISTEN    0         128                0.0.0.0:111              0.0.0.0:*        users:(("rpcbind",pid=26978,fd=4),("systemd",pid=1,fd=78))                     
LISTEN    0         128                0.0.0.0:20048            0.0.0.0:*        users:(("rpc.mountd",pid=26991,fd=8))                                          
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*        users:(("sshd",pid=874,fd=5))                                                  
LISTEN    0         64                    [::]:2049                [::]:*                                                                                       
LISTEN    0         64                    [::]:41763               [::]:*                                                                                       
LISTEN    0         80                       *:3306                   *:*        users:(("mysqld",pid=1180,fd=21))                                              
LISTEN    0         128                   [::]:48907               [::]:*        users:(("rpc.statd",pid=26983,fd=12))                                          
LISTEN    0         128                   [::]:111                 [::]:*        users:(("rpcbind",pid=26978,fd=6),("systemd",pid=1,fd=80))                     
LISTEN    0         128                   [::]:20048               [::]:*        users:(("rpc.mountd",pid=26991,fd=10))                                         
LISTEN    0         128                      *:80                     *:*        users:(("httpd",pid=944,fd=4),("httpd",pid=943,fd=4),("httpd",pid=942,fd=4),("httpd",pid=869,fd=4))
LISTEN    0         128                   [::]:22                  [::]:*        users:(("sshd",pid=874,fd=7))  
```
# Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux
**🌞 Install'**
```bash=
[zqo@web ~]$ sudo dnf install nfs-utils
[sudo] Mot de passe de zqo : 
[...]
Installé:
  gssproxy-0.8.0-19.el8.x86_64         keyutils-1.5.10-9.el8.x86_64             
  libevent-2.1.8-5.el8.x86_64          libverto-libevent-0.3.0-5.el8.x86_64     
  nfs-utils-1:2.3.3-46.el8.x86_64      rpcbind-1.2.5-8.el8.x86_64               

Terminé !
```
**🌞 Conf'**
```bash=
[zqo@web ~]$ sudo mkdir /srv/backup
```
```bash=
[zqo@web ~]$ sudo cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp6.linux

```
**🌞 Montage !**
```bash=
sudo mount -t nfs 10.5.1.13:/mnt/backup/web.tp6.linux/ /srv/backup
```
```bash=
[zqo@web ~]$ sudo df -h 
...
10.5.1.13:/mnt/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```
```bash=
[zqo@web ~]$ cat /etc/fstab
...
10.5.1.13:/mnt/backup/web.tp6.linux /srv/backup nfs     defaults        0 0
```
```bash=
[zqo@web backup]$ sudo mount -av
/                         : ignoré
/boot                     : déjà monté
none                      : ignoré
/srv/backup               : déjà monté
```
**🌞 Répétez les opérations sur db.tp6.linux**
```bash=
[zqo@db ~]$ sudo mkdir /srv/backup
```
```bash=
[zqo@db srv]$ sudo mount -t nfs 10.5.1.13:/mnt/backup/db.tp6.linux/ /srv/backup
```
```bash=
[zqo@db srv]$ sudo df -h
...
10.5.1.13:/mnt/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```
```bash=
[zqo@db backup]$ sudo mount -av
/                         : ignoré
/boot                     : déjà monté
none                      : ignoré
/srv/backup               : déjà monté
```
# Partie 4 : Scripts de sauvegarde
## I. Sauvegarde Web
**🌞 Ecrire un script qui sauvegarde les données de NextCloud**
```bash=
[zqo@web]$ sudo mkdir /var/log/backup
[sudo] Mot de passe de zqo : 
```
**🌞 Créer un service**

**🌞 Vérifier que vous êtes capables de restaurer les données**

**🌞 Créer un timer**

## II. Sauvegarde base de données
**🌞 Ecrire un script qui sauvegarde les données de la base de données MariaDB**

**🌞 Créer un service**

**🌞 Créer un timer**
