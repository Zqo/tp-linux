# TP1 : Are you dead yet ?

---

**:sun_with_face: Voici mes multiples façons d'exploser un linux :**

➜ Première Technique : 

Supprimer le dossier racine :

```  
zqo@zqo-VirtualBox:~$ rm -r / --no-preserve-root
```

➜ Deuxième Technique : 

Retirer tous les droits au dossier systemd :

```
zqo@zqo-VirtualBox:~$ chmod 000 /lib/systemd/systemd
```

➜ Troisième Technique : 

Rendre les périphériques (souris/clavier) inutilisable : 

```
zqo@zqo-VirtualBox:~$ xinput disable 11
zqo@zqo-VirtualBox:~$ xinput disable 12
```

➜ Quatrième Technique : 

Saturer la ram de la machine : 

Il faut tout d'abbord aller dans : 
        - Session and Startup
        - Application Autostart
        - Add
        - command : exo-open --launch TerminalEmulator

Ensuite dans le terminal : 

```
zqo@zqo-VirtualBox:~$ sudo nano ~/.bashrc


```

Ecrire "shutdown now" dans le nano

➜ Cinquième Technique : 

Supprimer l'init (premier process lancé par linux) dans le dossier sbin. Puis de créer un nouveau lien pour que l'init renvoie vers echo et pas vers systemd.

```
zqo@zqo-VirtualBox:~$ cd /sbin
zqo@zqo-VirtualBox:~$ sudo rm init
zqo@zqo-VirtualBox:~$ sudo ln -s /usr/bin/echo init
```