# II. Checklist

**🌞 Choisissez et définissez une IP à la VM**
* contenu de votre fichier de conf
```
NAME=enp0s8         
DEVICE=enp0s8        
BOOTPROTO=static    
ONBOOT=yes          
IPADDR=10.250.1.22
NETMASK=255.255.255.0
```
* Et le résultat d'un "ip a" pour me prouver que les changements on pris effet
```
: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a9:67:12 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.22/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
```
➜ Connexion SSH fonctionnelle
```
theorivry@theos-mbp ~ % ssh zqo@10.250.1.22
zqo@10.250.1.22's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Nov 23 13:48:31 2021 from 10.250.1.1
```

**🌞 Vous me prouverez que :**
* Le service ssh est actif sur la VM
```
[zqo@localhost ~]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset>
   Active: active (running) since Tue 2021-11-23 11:23:54 EST; 3h 11min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 869 (sshd)
    Tasks: 1 (limit: 4956)
   Memory: 3.9M
   CGroup: /system.slice/sshd.service
           └─869 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-po>

nov. 23 11:23:54 localhost.localdomain systemd[1]: Starting OpenSSH server daem>
nov. 23 11:23:54 localhost.localdomain sshd[869]: Server listening on 0.0.0.0 p>
nov. 23 11:23:54 localhost.localdomain sshd[869]: Server listening on :: port 2>
nov. 23 11:23:54 localhost.localdomain systemd[1]: Started OpenSSH server daemo>
nov. 23 13:48:31 localhost.localdomain sshd[1766]: Accepted password for zqo fr>
nov. 23 13:48:31 localhost.localdomain sshd[1766]: pam_unix(sshd:session): sess>
nov. 23 14:30:27 localhost.localdomain sshd[1820]: Accepted password for zqo fr>
nov. 23 14:30:27 localhost.localdomain sshd[1820]: pam_unix(sshd:session): sess>
```

**🌞 Prouvez que vous avez un accès internet**

* avec une commande ping
```
[zqo@localhost ~]$ ping 9.9.9.9
PING 9.9.9.9 (9.9.9.9) 56(84) bytes of data.
64 bytes from 9.9.9.9: icmp_seq=1 ttl=63 time=16.4 ms
```

**🌞 Prouvez que vous avez de la résolution de nom**
* Un petit ping vers un nom de domaine
```
[zqo@localhost ~]$ ping en-marche.fr
PING en-marche.fr (104.22.62.216) 56(84) bytes of data.
64 bytes from 104.22.62.216 (104.22.62.216): icmp_seq=1 ttl=63 time=18.1 ms
```

**🌞 Définissez node1.tp4.linux comme nom à la machine**

* Contenu du fichier /etc/hostname
```
[zqo@node1 ~]$ sudo nano /etc/hostname
node1.tp4.linux
```
* Commande hostname
```
[zqo@node1 ~]$ hostname
node1.tp4.linux
```

# III. Mettre en place un service

## 2. Install

**🌞 Installez NGINX**
```
[zqo@node1 ~]$ sudo dnf install nginx
Dernière vérification de l’expiration des métadonnées effectuée il y a 3:50:20 le mar. 23 nov. 2021 13:17:12 EST.
Le paquet nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64 est déjà installé.
Dépendances résolues.
Rien à faire.
Terminé `
```

## 3. Analyse

**🌞 Analysez le service NGINX**

* Avec une commande ps
```
[zqo@node1 ~]$ sudo ps -ef | grep nginx
root        4668       1  0 17:12 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       4669    4668  0 17:12 ?        00:00:00 nginx: worker process
zqo         4677    2004  0 17:16 pts/0    00:00:00 grep --color=auto nginx
```
* Avec une commande ss
```
[zqo@node1 ~]$ sudo ss -ltpn | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=4669,fd=8),("nginx",pid=4668,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=4669,fd=9),("nginx",pid=4668,fd=9))
```
* En regardant la conf
```
[zqo@node1 ~]$ cat /etc/nginx/nginx.conf
        root         /usr/share/nginx/html;
```
* Inspectez les fichiers de la racine web
```
[zqo@node1 ~]$ ls -la /usr/share/nginx/html/
total 20
drwxr-xr-x. 2 root root   99 23 nov.  17:07 .
drwxr-xr-x. 4 root root   33 23 nov.  17:07 ..
-rw-r--r--. 1 root root 3332 10 juin  05:09 404.html
-rw-r--r--. 1 root root 3404 10 juin  05:09 50x.html
-rw-r--r--. 1 root root 3429 10 juin  05:09 index.html
-rw-r--r--. 1 root root  368 10 juin  05:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 10 juin  05:09 poweredby.pn`
```

## 4. Visite du service web

**🌞 Configurez le firewall**
```
[zqo@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] Mot de passe de zqo : 
success
```
```
[zqo@node1 ~]$ sudo firewall-cmd --reload
success
```
**🌞 Tester le bon fonctionnement du service**

* Requêtes HTTP depuis le terminal
```
[zqo@node1 ~]$ curl http://10.250.1.22
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```
## 5. Modif de la conf du serveur web
**🌞 Changer le port d'écoute**
```
[zqo@node1 ~]$ cat /etc/nginx/nginx.conf | grep listen
        listen       8080 default_server;
        listen       [::]:8080 default_server;
```
```
[zqo@node1 ~]$ sudo systemctl restart nginx
[sudo] Mot de passe de zqo : 
[zqo@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor pres>
   Active: active (running) since Tue 2021-11-23 17:50:13 EST; 9s ago
  Process: 4744 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 4742 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 4740 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status>
 Main PID: 4746 (nginx)
    Tasks: 2 (limit: 4956)
   Memory: 3.6M
   CGroup: /system.slice/nginx.service
           ├─4746 nginx: master process /usr/sbin/nginx
           └─4747 nginx: worker process

nov. 23 17:50:13 node1.tp4.linux systemd[1]: nginx.service: Succeeded.
nov. 23 17:50:13 node1.tp4.linux systemd[1]: Stopped The nginx HTTP and reverse>
nov. 23 17:50:13 node1.tp4.linux systemd[1]: Starting The nginx HTTP and revers>
nov. 23 17:50:13 node1.tp4.linux nginx[4742]: nginx: the configuration file /et>
nov. 23 17:50:13 node1.tp4.linux nginx[4742]: nginx: configuration file /etc/ng>
nov. 23 17:50:13 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID>
nov. 23 17:50:13 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse>
```
``` 
[zqo@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
[sudo] Mot de passe de zqo : 
success
```
```
[zqo@node1 ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
```
```
[zqo@node1 ~]$ sudo firewall-cmd --reload
success
```
```
[zqo@node1 ~]$ sudo ss -ltpn | grep nginx
LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=4747,fd=8),("nginx",pid=4746,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=4747,fd=9),("nginx",pid=4746,fd=9))
```
```
[zqo@node1 ~]$ curl http://10.250.1.22:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```
**🌞 Changer l'utilisateur qui lance le service**
```
[zqo@node1 ~]$ sudo useradd web -m -s /bin/sh -u 2000
[sudo] Mot de passe de zqo : 
```
```
[zqo@node1 ~]$ sudo passwd web
Changement de mot de passe pour l'utilisateur web.
Nouveau mot de passe : 
MOT DE PASSE INCORRECT : Le mot de passe comporte moins de 8 caractères
Retapez le nouveau mot de passe : 
passwd : mise à jour réussie de tous les jetons d'authentification`
```
```
[zqo@node1 ~]$ cat /etc/nginx/nginx.
[...]
```
```
[zqo@node1 ~]$ ps -ef | grep nginx
root        4859       1  0 18:15 ?        00:00:00 nginx: master process /usr/sbin/nginx
web         4860    4859  0 18:15 ?        00:00:00 nginx: worker process
zqo         4865    2004  0 18:16 pts/0    00:00:00 grep --color=auto nginx
```

**🌞 Changer l'emplacement de la racine Web**
```
[zqo@node1 /]$ sudo mkdir /var/www
[zqo@node1 /]$ cd /var/www
[zqo@node1 var]$ sudo chmod -R 777 www/
[zqo@node1 var]$ sudo su - web
[web@node1 ~]$ cd /var/www/
[web@node1 www]$ mkdir super_site_web
```
```
[web@node1 www]$ ls -l
total 0
drwxrwxr-x. 2 web web 6 Nov 23 18:30 super_site_web
total 4
drwxrwxrwx.  3 root root   28 Nov 23 18:30 .
drwxr-xr-x. 22 root root 4096 Nov 23 18:18 ..
drwxrwxr-x.  2 web  web    24 Nov 23 18:33 super_site_web
[web@node1 www]$ ls -la super_site_web/
total 4
drwxrwxr-x. 2 web  web  24 Nov 23 18:33 .
drwxrwxrwx. 3 root root 28 Nov 23 18:30 ..
-rw-rw-r--. 1 web  web  45 Nov 23 18:33 index.html
```
```
[zqo@node1 var]$ cat /etc/nginx/nginx.conf
    server {
        listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web;
```
```
[zqo@node1 var]$ curl http://10.250.1.22:8080/
<!DOCTYPE html>
<html>
<h1>toto</h1>
</html>
```
<img src = "https://c.tenor.com/DE-LrMdvq_UAAAAC/chat-tout-mou.gif">