# TP2 : Manipulation de services

---

**1. Nommer la machine**

**🌞 Changer le nom de la machine**
* Première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine
```
zqo@zqo:~$ sudo hostname node1.tp2.linux
```

**Reload Terminal**

* Deuxième étape : changer le nom qui est pris par la machine quand elle s'allume
```
zqo@node1:~$ sudo nano /etc/hostname
zqo-node1.tp2.linux
```

```
zqo@node1:~$ cat /etc/hostname
zqo-node1.tp2.linux
```

**Redémarrer la machine**
```
zqo@node1:~$ ip a
192.168.57.5
```

**2. Config réseau**

**🌞 Config réseau fonctionnelle**

* Depuis la VM : ping 1.1.1.1 fonctionnel
```
zqo@node1:~$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data. 
64 bytes from 1.1.1.1:1icmp seq=] tt]=63 time=23.4 ms
```

* Depuis la VM : ping ynov.com fonctionnel
```
zqo@node1:~$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data. 
164 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp seq=] ttl=63 time=20.3 ms
```

* Depuis votre PC : ping <IP_VM> fonctionnel
```
zqo@nodel:~$ ping 192.168.57.1 
PING 192.168.57.1 (192.168.57.1) 56(84) bytes of data. 
64 bytes from 192.168.57.1: icmp seq=1 ttl=64 time=0.213 ms AC 
— 192.168.57.1 ping statistics —
1 packets transmitted, received, 0% packet loss, time Oms tt min/avg/max/mdev= 0.213/0.213/0.213/0.000 ms
```

# Partie 1 : SSH

**1. Installation du serveur**

**🌞 Installer le paquet openssh-server**

```
zqo@node1:~$ sudo apt install openssh-server
```


**2. Lancement du service SSH**

**🌞 Lancer le service ssh**
    
* Avec une commande systemctl start

```
zqo@node1:~$ systemctl start ssh
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'ssh.service'.
Authenticating as: zqo,,, (zqo)
Password: 
==== AUTHENTICATION COMPLETE ===
```
**3. Etude du service SSH**

**🌞 Analyser le service en cours de fonctionnement**

* Afficher le statut du service
```
zqo@node1:~$ systemctl status
 node1.tp2.linux 
 State: running
 Jobs: 0 queued
 Failed: 0 units
```

* Afficher le/les processus liés au service ssh
```
zqo@node1:~$ ps -ef
root   546     1  0 15:37 ?     00:00:00 sshd: /usr/sbin/sshd/ -D [listener] 0 of 10-100 start
```

* Afficher le port utilisé par le service ssh
```
zqo@node1:~$ ss -l
tcp LISTEN 0   128         0.0.0.0:ssh    0.0.0.0:*
```

* Afficher les logs du service ssh
```
zqo@node1:~$ journalctl -xe -u sshd -f
-- Journal begins at Tue 2021-10-19 16:31:49 CEST. --
```

**🌞 Connectez vous au serveur**

```
theorivry@theos-macbook-pro ~ % ssh zqo@192.168.57.5
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-20-generic x86_64)
```


**4. Modification de la configuration du serveur**

**🌞 Modifier le comportement du service**
    
* C'est dans le fichier /etc/ssh/sshd_config

```
zqo@node1:~$ sudo nano /etc/ssh/sshd_config
Include /etc/ssh/sshd_config.d/*.conf
#Port 21
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

* Effectuez le modifications suivante : 
 
```
zqo@node1:~$ cat /etc/ssh/sshd_config
Include /etc/ssh/sshd_config.d/*.conf
Port 1666
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

* Pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
```
zqo@node1:~$ sudo ss -ltpn
LISTEN   0   128   [::]:1666   [::]:*
 users:(("sshd",pid=17711,fd=4))
```
 **🌞 Connectez vous sur le nouveau port choisi**
 ```
 theorivry@theos-macbook-pro ~ % ssh -p 1666 zqo@192.168.57.5
 Password...
 ```
 
#  Partie 2 : FTP
 
 
**1. Installation du serveur**

**🌞 Installer le paquet vsftpd**
 
```
zqo@node1:~$ sudo apt install vsftpd
Reading package lists... Done
Building dependency tree
Reading state information... Done
vsftpd is already the newest version (3.0.3-12).
0 upgraded, 0 newly installed, 0 to remove and 89 not upgraded.
```

**2. Lancement du service FTP**

**🌞 Lancer le service vsftpd**
```
zqo@node1:~$ systemctl start vsftpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'vsftpd.service'.
Authenticating as: zqo,,, (zqo)
Password: 
==== AUTHENTICATION COMPLETE ===
```

**3. Etude du service FTP**
 
**🌞 Analyser le service en cours de fonctionnement**
    
* Afficher le statut du service
```
zqo@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset>
     Active: active (running) since Fri 2021-11-05 20:38:08 CET; 2 days ago
   Main PID: 2387 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 524.0K
     CGroup: /system.slice/vsftpd.service
             └─2387 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 05 20:38:08 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 05 20:38:08 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
* Afficher le/les processus liés au service vsftpd
```
zqo@node1:~$ pidof vsftpd
2387
```
```
zqo@node1:~$ ps 2387
PID TTY      STAT   TIME COMMAND
2387 ?        Ss     0:00 /usr/sbin/vsftpd /etc/vsftpd.conf
```

* Afficher le port utilisé par le service vsftpd
```
zqo@node1:~$ sudo ss -ltpn
users:(("vsftpd",pid=2387,fd=3))
LISTEN    0         5                    [::1]:631                 [::]:*
```

* Afficher les logs du service vsftpd

*Avec une commande journalctl*
```
zqo@node1:~$ journalctl | grep vsftpd
nov. 05 20:38:04 node1.tp2.linux sudo[2211]:      zqo : TTY=pts/2 ; PWD=/home/zqo ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 05 20:38:08 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 05 20:38:08 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
nov. 05 20:46:54 node1.tp2.linux sudo[16942]:      zqo : TTY=pts/2 ; PWD=/home/zqo ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 05 20:47:34 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:16976:814774 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-units for system-bus-name::1.136 [systemctl start vsftpd] (owned by unix-user:zqo)
nov. 06 22:25:05 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:17110:889239 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-unit-files for system-bus-name::1.151 [systemctl enable vsftpd] (owned by unix-user:zqo)
nov. 06 22:25:10 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:17110:889239 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.reload-daemon for system-bus-name::1.151 [systemctl enable vsftpd] (owned by unix-user:zqo)
nov. 07 22:17:45 node1.tp2.linux sudo[17888]:      zqo : TTY=pts/1 ; PWD=/home/zqo ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 07 22:18:31 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:17922:2143617 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-units for system-bus-name::1.220 [systemctl start vsftpd] (owned by unix-user:zqo)
nov. 07 22:23:54 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:17946:2174961 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.manage-unit-files for system-bus-name::1.231 [systemctl enable vsftpd] (owned by unix-user:zqo)
nov. 07 22:23:58 node1.tp2.linux polkitd(authority=local)[456]: Operator of unix-process:17946:2174961 successfully authenticated as unix-user:zqo to gain ONE-SHOT authorization for action org.freedesktop.systemd1.reload-daemon for system-bus-name::1.231 [systemctl enable vsftpd] (owned by unix-user:zqo)
```
*En consultant un fichier dans /var/log/*
```
zqo@node1:~$ sudo cat /var/log/vsftpd.log
[sudo] password for zqo: 
Mon Nov  8 14:08:51 2021 [pid 18756] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 14:08:52 2021 [pid 18755] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:00 2021 [pid 18861] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:00 2021 [pid 18860] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18870] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18869] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18871] [zqo] OK UPLOAD: Client "::ffff:192.168.57.1", "/home/zqo/access_log", 1105 bytes, 91.40Kbyte/sec
```
**🌞 Connectez vous au serveur**

* Depuis votre PC, en utilisant un client FTP
```
 Hôte : 192.168.57.5
 Identifiant : zqo
 MDP : ********
 Port : 21
```

* Vérifier que l'upload fonctionne
```
-
```

**🌞 Visualiser les logs**

* Mettez en évidence une ligne de log pour un download
```
Mon Nov  8 19:10:27 2021 [pid 29647] [zqo] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/zqo/TP1.txt", 0.00Kbyte/sec

```

* Mettez en évidence une ligne de log pour un upload
```
Mon Nov  8 19:05:09 2021 [pid 29603] [zqo] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/zqo/TP1.txt", 0.00Kbyte/sec
```


**4. Modification de la configuration du serveur**

**🌞 Modifier le comportement du service**

* C'est dans le fichier /etc/vsftpd.conf
* Effectuez les modifications suivantes :
     * Changer le port où écoute vstfpd
    * Peu importe lequel, il doit être compris entre 1025 et 65536
vous me prouverez avec un cat que vous avez bien modifié cette ligne 
```
zqo@node1:~$ sudo cat /etc/vsftpd.conf 
listen_port=1667

zqo@node1:~$ ss -l | grep 1667
LISTEN  0     32      *:1667     *:*      users:(("vsftpd",pid=29683,fd=3))                
```

* Pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
```
zqo@node1:~$ sudo cat /etc/vsftpd.conf
#
# Uncomment this to enable any form of FTP write command.
write_enable=YES
```

**🌞 Connectez vous sur le nouveau port choisi**
* Depuis votre PC, avec un client FTP
```
zqo@node1:~$ ftp 192.168.57.5 1667
Connected to 192.168.57.5.
SSH-2.0-OpenSSH_8.2p1 Ubuntu-4ubuntu0.3
```

* Re-tester l'upload et le download
```
zqo@node1:~$ sudo cat /var/log/vsftpd.log
Mon Nov  8 14:08:51 2021 [pid 18756] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 14:08:52 2021 [pid 18755] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:00 2021 [pid 18861] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:00 2021 [pid 18860] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18870] CONNECT: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18869] [zqo] OK LOGIN: Client "::ffff:192.168.57.1"
Mon Nov  8 15:16:11 2021 [pid 18871] [zqo] OK UPLOAD: Client "::ffff:192.168.57.1", "/home/zqo/access_log", 1105 bytes, 91.40Kbyte/sec
```

# Partie 3 : Création de votre propre service

**🌞 Donnez les deux commandes pour établir ce petit chat avec netcat**

* La commande tapée sur la VM
```
zqo@node1:~$ nc -l 1028
```

* La commande tapée sur votre PC
```
theorivry@theos-macbook-pro ~ % nc 192.168.57.5 1668
```

**🌞 Utiliser netcat pour stocker les données échangées dans un fichier**

```
zqo@node1:~/Desktop$ nc -l 25565 > netcat.txt
zqo@node1:~/Desktop$ cat netcat.txt
Test
```

**1.Créer le service**

**🌞 Créer un nouveau service**

```
zqo@node1:/etc/systemd/system$ sudo nano chat_tp2.service
zqo@node1:/etc/systemd/system$ sudo chmod 777 chat_tp2.service
zqo@node1:/etc/systemd/system$ ls -al
[...]
-rwxrwxrwx  1 root root  122 nov.   8 21:32 chat_tp2.service
[...]
zqo@node1:/etc/systemd/system$ which nc
/usr/bin/nc
zqo@node1:/etc/systemd/system$ cat chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 1668

[Install]
WantedBy=multi-user.target

```
**2. Test test et retest**

**🌞 Tester le nouveau service**

```
zqo@node1:~$ sudo systemctl start chat_tp2

zqo@node1:~$ systemctl status chat_tp2
● chat_tp2.service - Little chat service (TP2)
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 21:35:34 CET; 10s ago
   Main PID: 1908 (nc)
      Tasks: 1 (limit: 2312)
     Memory: 192.0K
     CGroup: /system.slice/chat_tp2.service
             └─1908 /usr/bin/nc -l 1668

nov. 08 21:35:34 node1.tp2.linux systemd[1]: Started Little chat service (TP2).

zqo@node1:$ ss -l
tcp   LISTEN 0      1                                         0.0.0.0:1668                    0.0.0.0:*

```
